FROM alpine:latest

# [REQUIRED]
COPY  /opt/* /opt/
ENV PATH="/opt:${PATH}"

RUN \
  # [REQUIRED]
  apk add --no-cache ca-certificates openssl  && \
  \
  # [TOOL] tool
  apk add --no-cache \
    curl \
    openssh-client
