# Example to update custom trust site CA certificates

## Build image

Update trust-site in `/opt/trust-sites` file

```
docker build -t test .
```

## Testing

**Start Docker** 

```
docker run -it --rm \
-v $(pwd)/test-resources/kube-ca.crt:/run/secrets/kubernetes.io/serviceaccount/ca.crt \
-e "APISERVER=api_server_from_kube_config" \
-e "TOKEN=token_from_kube_config \
test sh

```

**Update trust site** 

```
update-trust-sites
```

**Test**

Running curl to site without `-k` 
```
curl -i "https://example.com:8443"
```
